import { Animal } from './Animal';
import { Walkable } from './Walkable';
export class Dog implements Animal, Walkable {
  public walk() {
    console.log('dog can walk');
  }
}
