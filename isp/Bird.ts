import { Animal } from './Animal';
import { Flyable } from './Flyable';
import { Walkable } from './Walkable';

export class Bird implements Animal, Flyable, Walkable {
  public walk() {
    console.log('bird can walk');
  }

  public fly() {
    console.log('bird can fly');
  }
}
