import { IPost } from './IPost';
import { IDatabase } from './IDatabase';

export class OfficeUser {
  private db: IDatabase;
  private post: IPost;

  constructor(db: IDatabase, post: IPost) {
    this.db = db;
    this.post = post;
  }

  public publishNewPost() {
    let postMessage: String = 'example message';
    this.post.createPost(this.db, postMessage);
  }
}
