import { EmailIntegrationHandler } from './EmailIntegrationHandler';
import { SMSIntegrationHandler } from './SMSIntegrationHandler';
import { PushIntegrationHandler } from './PushIntegrationHandler';
import { IntegrationHandler } from './IntegrationHandler';

export class IntegrationHandlerFactory {
  private static EMAIL: String = 'abc';

  private static SMS: String = 'dce';

  private static PUSH: String = 'glk';

  private emailHandler: EmailIntegrationHandler;

  private smsHandler: SMSIntegrationHandler;

  private pushHandler: PushIntegrationHandler;

  public constructor(
    emailHandler: EmailIntegrationHandler,
    smsHandler: SMSIntegrationHandler,
    pushHandler: PushIntegrationHandler
  ) {
    this.emailHandler = emailHandler;
    this.smsHandler = smsHandler;
    this.pushHandler = pushHandler;
  }

  public getHandlerForEmail(integration: String): IntegrationHandler {
    return this.emailHandler;
  }

  public getHandleForSMS(integration: String): IntegrationHandler {
    return this.smsHandler;
  }

  public getHandlerForPush(integration: String): IntegrationHandler {
    return this.pushHandler;
  }
}
