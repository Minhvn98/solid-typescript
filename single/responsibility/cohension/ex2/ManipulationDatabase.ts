import { Connection, DriverManager, Statement } from '../database-util';

export class ManipulationDatabase {
  public saveToDatabase(): boolean {
    let url: String = 'jdbc:msql://xxx.yyyy.zzz.lll:3306/Demo';
    let conn: Connection = DriverManager.getConnection(url, '', '');
    let st: Statement = conn.createStatement();
    st.executeUpdate(
      'INSERT INTO Customers ' +
        "VALUES (1001, 'Simpson', 'Mr.', 'Springfield', 2001)"
    );
    return true;
  }
}
