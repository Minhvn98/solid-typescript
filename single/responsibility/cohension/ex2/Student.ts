export class Student {
  id: number;

  name: String;

  age: number;

  public getId(): number {
    return this.id;
  }

  public setId(id: number) {
    this.id = id;
  }

  public getName(): String {
    return this.name;
  }

  public setName(name: String) {
    this.name = name;
  }

  public getAge(): number {
    return this.age;
  }

  public setAge(age: number) {
    this.age = age;
  }
}
