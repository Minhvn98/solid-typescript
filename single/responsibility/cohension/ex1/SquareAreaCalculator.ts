import { Square } from './Square';
class SquareAreaCalculator extends Square {
  public calculateArea(): number {
    var realSquare = this.size * this.size;
    return realSquare;
  }

  public calculateP(): number {
    var realP = this.size * 4;
    return realP;
  }
}
