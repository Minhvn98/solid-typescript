export class Square {
  private _size: number;

  get size() {
    return this._size;
  }

  set size(size: number) {
    this._size = size;
  }
}
