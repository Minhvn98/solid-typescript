import { FileUtil } from './FileUtil';
import { Database } from './Database';

export class Post {
  public CreatePost(db: Database, postMessage: String) {
    try {
      db.Add(postMessage);
    } catch (ex: Error) {
      db.logError('An error occured: ', ex.toString());
      FileUtil.writeAllText('LocalErrors.txt', ex.toString());
    }
  }
}
