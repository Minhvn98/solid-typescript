export class ChaosCondition {
  private isUpdateReady: boolean;

  private isSynchCompleted: boolean;

  private isCacheEnabled: boolean;

  private updateDb(isForceUpdate: boolean) {
    //  isUpdateReady is class level
    //  variable
    if (!this.isUpdateReady) return;
    //  isForceUpdate is argument variable
    //  and based on this inner blocks is
    //  executed
    if (!isForceUpdate) return this.updateCache(!this.isCacheEnabled);

    this.updateBackupDb(true);

    if (this.isSynchCompleted) return this.updateDbMain(true);

    this.updateDbMain(false);
  }

  private updateCache(b: boolean) {}

  private updateBackupDb(b: boolean) {}

  private updateDbMain(b: boolean) {}
}
