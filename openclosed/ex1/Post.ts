import { Database } from '../../single/responsibility/cohension/ex3/Database';
import { Tag } from './Tag';

export class Post implements Tag {
  CreatePost(db: Database, postMessage: string) {
    if (postMessage.startsWith('#')) {
      db.addAsTag(postMessage);
    }

    if (postMessage.startsWith('mention')) {
      db.addAsMentionPost(postMessage);
    } else {
      db.Add(postMessage);
    }
  }

  createTag() {}
}
