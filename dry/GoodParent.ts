export class GoodParent {
  public findAllGoodPersonByGender(
    persons: Array<Person>,
    gender: Gender
  ): Array<Person> {
    return persons.filter(function (person) {
      person.name.startsWith('T') &&
        person.age > 25 &&
        person.hairColor == HairColor.BLACK &&
        person.skinColor == SkinColor.WHITE &&
        person.sex == gender;
    });
  }
}
class Person {
  name: String;

  age: number;

  sex: Gender;

  hairColor: HairColor;

  skinColor: SkinColor;
}
enum SkinColor {
  BLACK,

  BROWN,

  WHITE,
}
enum HairColor {
  RED,

  BLACK,

  YELLOW,
}
enum Gender {
  FEMALE,

  MALE,

  GAY,

  LES,
}
